#!/bin/sh

cordova build --release android
sudo cp my-release-key.keystore platforms/android/build/outputs/apk/
cd platforms/android/build/outputs/apk
sudo jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore android-release-unsigned.apk alias_name
zipalign -v 4 android-release-unsigned.apk "$1.apk"
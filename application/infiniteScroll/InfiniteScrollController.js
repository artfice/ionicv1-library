var InfiniteScrollController = function($scope, contentService) {
    this._scope = $scope;
    this.items = [];
    this._page = 1;
    this._noMoreItemsAvailable = false;
    this._noItemFound = false;
    this._contentService = contentService;

};

InfiniteScrollController.prototype.loadMore = function(){
    var self = this;
    this._contentService.getItems(this.page).then(function(items) {
        if (items.numPages == items.page){
            self._noMoreItemsAvailable = true;
            self._scope.$broadcast('scroll.infiniteScrollComplete');
        }
        if (items.numPages == 0 || items.total == 0){
            self._noMoreItemsAvailable = true;
            self._noItemFound = true;
        }
        self._page++;
        self.items = self.items.concat(items.list);
        self._scope.$broadcast('scroll.infiniteScrollComplete');
    });
};

InfiniteScrollController.prototype.doRefresh = function(){
    this.items = [];
    this._page = 1;
    this._noMoreItemsAvailable = false;
    this._noItemFound = false;
    this.loadMore();
    this._scope.$broadcast('scroll.refreshComplete');
};

module.exports = InfiniteScrollController;
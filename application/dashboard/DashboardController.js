var DashboardController = function(candidateService, $rootScope) {
    this._candidateService = candidateService;
    this._rootScope = $rootScope;
    this.candidates = this._candidateService.candidates;
    this.show = false;

    this.riding = [{
        name: 'Adam Vaughan',
        poll: '30.9%',
        party: 'Liberal',
        width: '30.9%'
    }, {
        name: 'Olivia Chow',
        poll: '21.7%',
        party: 'NDP',
        width: '21.7%'
    }, {
        name: 'Sabrina Zuniga',
        poll: '37.2%',
        party: 'Conservative',
        width: '37.2%'
    }, {
        name: 'Sharon Danley',
        poll: '4.4%',
        party: 'Green',
        width: '4.4%'
    }];


    this.canada = [{
        name: 'Justin Trudeau',
        poll: '31%',
        party: 'Liberal',
        width: '31%'
    }, {
        name: 'Thomas Mulcair',
        poll: '28%',
        party: 'NDP',
        width: '28%'
    }, {
        name: 'Stephen Harper',
        poll: '25%',
        party: 'Conservative',
        width: '25%'
    }, {
        name: 'Elizabeth May',
        poll: '4%',
        party: 'Green',
        width: '4%'
    }];

    this.button = {
        riding: true,
        canada: false
    };
    this.getStyle = function(oneCandidate) {
        return {
            'background': ('url(' + oneCandidate.img + ') no-repeat'),
            'background-size': 'cover',
            'background-position': 'center',
            'height': '80px'
        };
    };

    if ($rootScope.firstTime){
        this.show = true;
    }
};

DashboardController.prototype.ridingSelected = function(location) {
    this._rootScope.location = location;
    this.show = false;
    this._rootScope.firstTime = false;
};

module.exports = DashboardController;
//controller
var DashboardController = require('./DashboardController');

module.exports = angular.module('dashboardmodule', [])
                 .controller('dashboardController', ['candidateService', '$rootScope', DashboardController])
                 .controller('footerController', [function(){}])
                 .config(function ($stateProvider, $urlRouterProvider) {
                    $stateProvider.state('footer', {
                        templateUrl: 'dashboard/footer.html',
                        controller: 'footerController',
                        abstract: true
                    });

                    $stateProvider.state('footer.dashboard', {
                        url : '/dashboard',
                        templateUrl: 'dashboard/dashboard.html',
                        controller: 'dashboardController',
                        controllerAs: 'vm'
                    });
                });
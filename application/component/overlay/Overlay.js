var Overlay = function() {
    return {
        controller: ['$scope', function ($scope) {
        }],
        replace: true,
        controllerAs: 'vm',
        transclude: true,
        restrict: 'E',
        bindToController: {
            show: '='
        },
        scope: {},
        templateUrl: 'component/overlay/overlay.html'
    };
};

module.exports = Overlay;
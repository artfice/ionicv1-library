var IntroController = function($state , keys, analyticService, $scope, $compile, $ionicSlideBoxDelegate) {
  this._service = analyticService;
  this._keys = keys;
  this._state = $state;
  this.activeSlide = 0;
  this._map = [
     this._keys.INTRO.INTRO1,
     this._keys.INTRO.INTRO2,
     this._keys.INTRO.INTRO3,
     this._keys.INTRO.INTRO4,
     this._keys.INTRO.INTRO5
  ];
  this._scope = $scope;
  this._ionicSlideBoxDelegate = $ionicSlideBoxDelegate;
  setTimeout(function(){
    var myEl = angular.element(document.getElementsByClassName('slider-pager'));
    var skipHtml = $compile("<div class='skip' ng-click='vm.skip()'>Skip</div>")($scope);
    myEl.append(skipHtml);
  },0);

  this._service.saveAnalytic(this._map[0]);

  this._scope.$watch(function() {
      return this.activeSlide;
  }.bind(this),
    function(newValue, oldValue) {
      switch(newValue) {
        case 0:
        case 4:
            this._ionicSlideBoxDelegate.enableSlide(false);
            break;
      }
    }.bind(this)
  );

  this.enableSlide = function() {
      this._ionicSlideBoxDelegate.enableSlide(true);
  };
};

IntroController.prototype.skip = function() {
    this._service.saveAnalytic(this._keys.INTRO.SKIP);
    this._state.go('footer.dashboard', {});
};

IntroController.prototype.slideHasChanged = function(index) {
    this._service.saveAnalytic(this._map[index]);
};
module.exports = IntroController;
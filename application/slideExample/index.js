//controller
var IntroController = require('./IntroController');

//templates
// var introTemplate = require('./intro.html');

module.exports = angular.module('intromodule', [])
                 .controller('introController', ['$state', 'keys', 'analyticService', '$scope', '$compile', '$ionicSlideBoxDelegate', IntroController])
                 .config(function ($stateProvider, $urlRouterProvider) {
                    $stateProvider.state('intro', {
                        url : '/intro',
                        templateUrl: 'intro/intro.html',
                        controller: 'introController',
                        controllerAs: 'vm'
                    });
                });
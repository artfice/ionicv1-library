var html : string = `
<ion-view view-title="Intro">
  <ion-content  scroll="false" class="intro-component">
   <ion-slide-box on-slide-changed="vm.slideHasChanged($index)" active-slide="vm.activeSlide">
        <ion-slide on-drag-left="vm.enableSlide()">
            <div class="slide1">
                <img src="./img/intro/votr-smalllogo.png" class="logo"/>
                <img src="./img/intro/onboarding-icon1.png" class="icon-image"/>
                <div class="blurb">
                votr helps you to understand the issues being addressed by politicians in the current election.
                </div>
            </div>
        </ion-slide>
        <ion-slide >
            <div class="slide2">
                <img src="./img/intro/votr-smalllogo.png" class="logo"/>
                <img src="./img/intro/onboarding-icon2.png" class="icon-image"/>
                <div class="blurb">
                Deep Dive into important issues and see what candidates think about the issues that matter to you.
                </div>
            </div>
        </ion-slide>
        <ion-slide >
            <div class="slide3">
                <img src="./img/intro/votr-smalllogo.png" class="logo"/>
                <img src="./img/intro/onboarding-icon3.png" class="icon-image"/>
                <div class="blurb">
                To better understand your political alignment you can favourite stances that reflect your political beliefs.
                </div>
            </div>
        </ion-slide>
        <ion-slide >
            <div class="slide4">
                <img src="./img/intro/votr-smalllogo.png" class="logo"/>
                <img src="./img/intro/onboarding-icon4.png" class="icon-image"/>
                <div class="blurb">
                On Election day you can cast your ballot in app using your Voter ID and government issued PIN.
                </div>
            </div>
        </ion-slide>
        <ion-slide on-drag-right="vm.enableSlide()">
            <div class="slide5">
                <img src="./img/intro/votr-smalllogo.png" class="logo"/>
                <img src="./img/intro/onboarding-icon5.png" class="icon-image"/>
                <div class="blurb">
                While browsing Issues you can agree or disagree with party stances. Your decisions are counted and represented on the home page by candidate.
                </div>
            </div>
        </ion-slide>
    </ion-slide-box>
  </ion-content>
</ion-view>
`;

export = html;

var LocalStorageService = function($window) {
  this._window = $window;
};

LocalStorageService.prototype =  {
  set: function(key, value) {
    this._window.localStorage[key] = value;
  },
  get: function(key, defaultValue) {
    return this._window.localStorage[key] || defaultValue;
  },
  setObject: function(key, value) {
    this._window.localStorage[key] = JSON.stringify(value);
  },
  getObject: function(key) {
    return JSON.parse(this._window.localStorage[key] || '{}');
  }
};

module.exports = LocalStorageService;
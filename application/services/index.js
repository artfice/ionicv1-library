var keys = require('./keys');
var AnalyticService = require('./AnalyticService');
var HttpService = require('./HttpService');
var LocalStorageService = require('./LocalStorageService');

module.exports = angular.module('servicemodule', [])
                 .value('keys', keys)
                 .service('analyticService', ['$q', '$http', 'keys', AnalyticService]),
                 .service('httpService', ['$http', '$q', HttpService])
                 .service('localStorageService', ['$window', LocalStorageService]);